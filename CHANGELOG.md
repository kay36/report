# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

The release dates can be found on the [releases page](https://gitlab.com/gitlab-org/security-products/analyzers/vulnerability/-/releases).

## v3.12.2
- Update `common` to v3.2.0 (!37)

## v3.12.1
- Add `DetailsURLField` to `Details` (!34)

## v3.12.0
- Change the type of `Scan.Analyzer` to `AnalyzerDetails` (!30)

## v3.11.0
- Add `scan.analyzer` field to the security report (!28)

## v3.10.0
- Change `Details` field so that it can be used by client libraries without having to issue new versions of the report module (!24)
- Move Dependency Scanning `vulnerable_package` field to the Gemnasium analyzer (!24)

## v3.9.2
- Includes `suppressions` property during `.sarif` file serialization. (!26)
- Skips to include the finding in the artifact if a sarif result has `suppressions` array property, unless `suppressions[].status` is `underReview` or `rejected`. (!26)

## v3.9.1
- Add missing `PackageManager` constants for Go, Gradle, Pipenv, and others (!25)

## v3.9.0
- Add `Details` field to `Vulnerability` (!23)
- Add `DependencyVulnerablePackageDetails` field for vulnerable package data (!23)

## v3.8.0
- Implement ruleset overrides (!22)

## v3.7.1
- Drop misleading SARIF transformation warning (!21)

## v3.7.0
- Map sarif rule name to identifier.name (!20)

## v3.6.0
- Add support for sarif translation to gl-sast-report.json (!19)

## v3.5.0
- Add support for GHSA identifier type (!17)

## v3.4.1
- Avoid implicit memory aliasing within for loop (!16)

## v3.4.0
- Add `location` fields and `Category` for Cluster Image Scanning reports (!14)

## v3.3.0
- Omit `report.Remediations` if empty (!13)

## v3.2.1
- Bump `report.Version` to `v14.0.3` match latest report schema (!12)

## v3.2.0
- Add `tracking` to the vulnerability struct for post-analyzer processing (!10)

## v3.1.0
- Add `flags` to the vulnerability struct for post-analyzer processing (!9)

## v3.0.1
### Fix

- Fix module reference for major version bump (!6)

## v3.0.0
### Removed

- Removed unused WASC identifier support (!5)

## v2.1.0
### Changed

- Change report version from 3.0.0 to 14.0.0 (!4)

## v2.0.0
### Changed

- Rename `Issue` struct to `Vulnerability` (!2)

## v1.0.0
### Added

- Add code for implementing security scanners that generate [GitLab Security reports](https://gitlab.com/gitlab-org/security-products/security-report-schemas) (!1)
