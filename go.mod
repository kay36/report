module gitlab.com/gitlab-org/security-products/analyzers/report/v3

go 1.13

require (
	github.com/google/go-cmp v0.5.6
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.7.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v3 v3.2.0
	gitlab.com/gitlab-org/security-products/analyzers/ruleset v1.4.0
)
