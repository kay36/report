package report

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"regexp"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"
)

type sarif struct {
	Schema  string `json:"$schema"`
	Version string `json:"version"`
	Runs    []run  `json:"runs"`
}

type run struct {
	Invocations []invocation `json:"invocations"`
	Results     []result     `json:"results"`
	Tool        struct {
		Driver struct {
			Name            string `json:"name"`
			SemanticVersion string `json:"semanticVersion"`
			Rules           []rule `json:"rules"`
		} `json:"driver"`
	} `json:"tool"`
}

type invocation struct {
	ExecutionSuccessful        bool           `json:"executionSuccessful"`
	ToolExecutionNotifications []notification `json:"toolExecutionNotifications"`
}

type notification struct {
	Descriptor struct {
		ID string `json:"id"`
	} `json:"descriptor"`
	Level   string `json:"level"`
	Message struct {
		Text string `json:"text"`
	} `json:"message"`
}

type rule struct {
	ID               string `json:"id"`
	Name             string `json:"name"`
	ShortDescription struct {
		Text string `json:"text"`
	} `json:"shortDescription"`
	FullDescription struct {
		Text string `json:"text"`
	} `json:"fullDescription"`
	DefaultConfiguration struct {
		Level string `json:"level"`
	} `json:"defaultConfiguration"`
	Properties struct {
		Precision string   `json:"precision"`
		Tags      []string `json:"tags"`
	} `json:"properties"`
	HelpURI string `json:"helpUri"`
}

type result struct {
	RuleID  string `json:"ruleId"`
	Message struct {
		Text string `json:"text"`
	} `json:"message"`
	Locations []struct {
		PhysicalLocation struct {
			ArtifactLocation struct {
				URI       string `json:"uri"`
				URIBaseID string `json:"uriBaseId"`
			} `json:"artifactLocation"`
			Region struct {
				StartLine   int `json:"startLine"`
				StartColumn int `json:"startColumn"`
				EndLine     int `json:"endLine"`
				EndColumn   int `json:"endColumn"`
			} `json:"region"`
		} `json:"physicalLocation"`
	} `json:"locations"`
	Suppressions []struct {
		Kind   string `json:"kind"`             // values= 'inSource', 'external'
		Status string `json:"status,omitempty"` // values= empty,'accepted','underReview','rejected'
		GUID   string `json:"guid,omitempty"`
	} `json:"suppressions,omitempty"`
}

const vulnerabilityMessageMaxLength = 400

var supportedDrivers = []string{
	"KICS",
	"semgrep",
}
var tagIDRegex = regexp.MustCompile(`([^-]+)-([^:]+): (.+)`)

// TransformToGLSASTReport will take in a sarif file and output a GitLab SAST Report
func TransformToGLSASTReport(reader io.Reader, rootPath, analyzerID string, scanner Scanner) (*Report, error) {
	s := sarif{}

	jsonBytes, err := readerToBytes(reader)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(jsonBytes, &s)
	if err != nil {
		return nil, err
	}

	if s.Version != "2.1.0" {
		return nil, fmt.Errorf("version for SARIF is %s, but we only support 2.1.0", s.Version)
	}

	newReport := NewReport()
	var allVulns []Vulnerability

	// It is generally expected to only have a single run, but best to parse all as it is a collection.
	for _, run := range s.Runs {
		for i := range run.Invocations {
			invocation := run.Invocations[i]
			for n := range invocation.ToolExecutionNotifications {
				notification := invocation.ToolExecutionNotifications[n]
				if notification.Level == "error" {
					return &newReport, fmt.Errorf(notificationMsg(notification))
				}

				logNotification(notification)
			}
		}

		vulns, err := transformRun(run, rootPath, scanner)
		if err != nil {
			return nil, err
		}

		allVulns = append(allVulns, vulns...)
	}

	newReport.Analyzer = analyzerID
	newReport.Config.Path = ruleset.PathSAST
	newReport.Vulnerabilities = allVulns
	return &newReport, nil
}

func readerToBytes(reader io.Reader) ([]byte, error) {
	buf := new(bytes.Buffer)
	_, err := buf.ReadFrom(reader)
	if err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

// shouldSuppress indicates if the given finding(`result`) should be included
// in the artifact. This is determined based on the `suppressions` property from
// the sarif output file.
func shouldSuppress(r result) bool {
	if len(r.Suppressions) == 0 {
		return false
	}
	for _, sup := range r.Suppressions {
		// if one of the suppressions is under review or rejected, include
		// the finding in the artifact.
		if sup.Status == "underReview" || sup.Status == "rejected" {
			return false
		}
	}
	return true
}

func transformRun(r run, rootPath string, scanner Scanner) ([]Vulnerability, error) {
	ruleMap := make(map[string]rule)
	for _, rule := range r.Tool.Driver.Rules {
		ruleMap[rule.ID] = rule
	}

	var vulns []Vulnerability
	for _, result := range r.Results {
		if shouldSuppress(result) {
			continue
		}
		for _, location := range result.Locations {
			rule := ruleMap[result.RuleID]

			var description string
			if len(result.Message.Text) > vulnerabilityMessageMaxLength {
				description = result.Message.Text[:vulnerabilityMessageMaxLength]
			} else {
				description = result.Message.Text
			}

			lineEnd := location.PhysicalLocation.Region.EndLine

			vulns = append(vulns, Vulnerability{
				Description: description,
				Category:    CategorySast,
				Message:     message(rule),
				Severity:    severity(rule),
				Scanner:     scanner,
				Location: Location{
					File:      removeRootPath(location.PhysicalLocation.ArtifactLocation.URI, rootPath),
					LineStart: location.PhysicalLocation.Region.StartLine,
					LineEnd:   lineEnd,
				},
				Identifiers: identifiers(rule, scanner),
			})
		}
	}
	return vulns, nil
}

// See: https://docs.oasis-open.org/sarif/sarif/v2.1.0/os/sarif-v2.1.0-os.html#_Toc34317855 for more
// information about the level property. The docs say that when level is not defined, then the value is equal
// to warning.
func severity(r rule) SeverityLevel {
	switch r.DefaultConfiguration.Level {
	case "error":
		return SeverityLevelCritical
	case "warning":
		return SeverityLevelMedium
	case "note":
		return SeverityLevelInfo
	case "none":
		return SeverityLevelUnknown
	default:
		return SeverityLevelMedium
	}
}

func message(r rule) string {
	for _, tag := range r.Properties.Tags {
		splits := strings.Split(tag, ":")
		if strings.HasPrefix(splits[0], "CWE") {
			return strings.TrimLeft(splits[1], " ")
		}
	}

	// default to full text description
	return r.FullDescription.Text
}

func identifiers(r rule, scanner Scanner) []Identifier {
	ids := []Identifier{
		{
			Type:  IdentifierType(fmt.Sprintf("%s_id", scanner.ID)),
			Name:  r.Name,
			Value: r.ID,
			URL:   r.HelpURI,
		},
	}

	for _, tag := range r.Properties.Tags {
		matches := tagIDRegex.FindStringSubmatch(tag)

		if matches != nil {
			switch strings.ToLower(matches[1]) {
			case "cwe":
				cweID, err := strconv.Atoi(matches[2])
				if err != nil {
					log.Errorf("Failure to parse CWE ID: %v\n", err)
					continue
				}

				ids = append(ids, CWEIdentifier(cweID))
			default:
				ids = append(ids, Identifier{
					Type:  IdentifierType(strings.ToLower(matches[1])),
					Name:  matches[3],
					Value: matches[2],
				})
			}
		}
	}

	return ids
}

func removeRootPath(path, rootPath string) string {
	prefix := strings.TrimSuffix(rootPath, "/") + "/"
	if path[0] != '/' {
		prefix = strings.TrimPrefix(prefix, "/")
	}

	return strings.TrimPrefix(path, prefix)
}

func notificationMsg(notification notification) string {
	return fmt.Sprintf(
		"tool notification %s: %s %s",
		notification.Level,
		notification.Descriptor.ID,
		notification.Message.Text)
}

// https://docs.oasis-open.org/sarif/sarif/v2.1.0/csprd01/sarif-v2.1.0-csprd01.html#_Ref493404972
func logNotification(notification notification) {
	msg := notificationMsg(notification)
	switch notification.Level {
	case "error":
		log.Error(msg)
	case "warning":
		log.Warn(msg)
	case "note":
		log.Info(msg)
	case "none":
		log.Debug(msg)
	default:
		log.Debug(msg)
	}
}
