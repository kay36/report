package report

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"

	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"
)

var (
	scanner = Scanner{
		ID:   "AnalyzerID",
		Name: "AnalyzerName",
	}
)

func TestUnsupportedSarifVersion(t *testing.T) {
	minimalSarif := `
		{
			"version": "2.0.0"
		}
	`

	report := strings.NewReader(minimalSarif)
	_, err := TransformToGLSASTReport(report, "/tmp/app", "analyzerID", scanner)

	expected := fmt.Errorf("version for SARIF is 2.0.0, but we only support 2.1.0")

	if err == nil || err.Error() != expected.Error() {
		t.Errorf("expected to get: %v, but got: %v", expected, err)
	}
}

func TestTransformToGLSASTReport(t *testing.T) {
	tests := []struct {
		name       string
		reportPath string
		wantReport string
	}{
		{
			name:       "semgrep sarif example",
			reportPath: "testdata/reports/semgrep.sarif",
			wantReport: "testdata/expect/sarif.semgrep.gl-sast-report.json",
		},
		{
			name:       "KICS sarif example",
			reportPath: "testdata/reports/kics.sarif",
			wantReport: "testdata/expect/sarif.kics.gl-sast-report.json",
		},
		{
			name:       "semgrep sarif excluding suppressions",
			reportPath: "testdata/reports/semgrep_with_suppressions.sarif",
			wantReport: "testdata/expect/sarif.semgrep.excl-suppr.gl-sast-report.json",
		},
	}

	for idx := range tests {
		tt := tests[idx]
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			report, err := os.Open(tt.reportPath)
			if err != nil {
				t.Fatal(err)
			}
			defer report.Close()

			got, err := TransformToGLSASTReport(report, "/tmp/app", "analyzerID", scanner)
			if err != nil {
				t.Fatal(err)
			}

			// Analyzer and Config are not set in json
			got.Analyzer = ""
			got.Config = ruleset.Config{}
			// Remediations and DependencyFiles are nil as they were omitted from json report for being empty
			got.Remediations = nil
			got.DependencyFiles = nil

			var want Report
			wantReportBytes, err := ioutil.ReadFile(tt.wantReport)
			if err != nil {
				t.Fatal(err)
			}

			err = json.Unmarshal(wantReportBytes, &want)
			if err != nil {
				t.Fatal(err)
			}

			if diff := cmp.Diff(&want, got); diff != "" {
				t.Errorf("Wrong result. (-want +got):\n%s", diff)
			}
		})
	}
}

func TestToolExecutionNotifications(t *testing.T) {
	tests := []struct {
		path   string
		err    bool
		errMsg string
	}{
		{"testdata/reports/semgrep_invalid_config.sarif", true, "tool notification error: InvalidRuleSchemaError Additional properties are not allowed ('pattern-eitherrr' was unexpected)"},
		{"testdata/reports/semgrep_js_syntax_error.sarif", false, ""},
		{"testdata/reports/semgrep_nonmatching_nosem.sarif", false, ""},
	}

	for idx := range tests {
		tt := tests[idx]
		t.Run(tt.path, func(t *testing.T) {

			fixture, err := os.Open(tt.path)
			if err != nil {
				t.Fatal(err)
			}

			_, err = TransformToGLSASTReport(fixture, "/tmp/app", "analyzerID", scanner)
			if tt.err && err == nil {
				t.Error("expected an error, but it was nil")
				return
			}

			if tt.err && tt.errMsg != err.Error() {
				t.Errorf("expected: %v\n but got: %v", tt.errMsg, err.Error())
			}
		})
	}
}

func TestRemoveRootPath(t *testing.T) {
	tests := []struct {
		path     string
		rootPath string
		expected string
	}{
		{"/a/b/c/d.foo", "/a/b/", "c/d.foo"},
		{"/a/b/c/d.foo", "/a/b", "c/d.foo"},
		{"/a/b/c/d.foo", "/a/c", "/a/b/c/d.foo"},
		{"a/b/c/d.foo", "/a/b", "c/d.foo"},
	}

	for _, tt := range tests {
		got := removeRootPath(tt.path, tt.rootPath)

		if got != tt.expected {
			t.Errorf("expected: %s, but got: %s", tt.expected, got)
		}
	}
}
